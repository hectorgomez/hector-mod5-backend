const http = require('http');
const mysql = require('mysql2/promise');

const server = http.createServer();

let conn;

(async () => {
    conn = await mysql.createConnection({
    host: 'localhost',
    user: 'root',
    database: 'test',
    password: 'pw',
  });
})();

server.on("request", async (req, res) => {
  if (req.url === "/foo" && req.method.toUpperCase() === "GET") {
    try {
      res.writeHead(200, { "Content-Type": "application/json"});
  
      const query = 'SELECT * FROM `test`';
      const [result] = await conn.execute(query);
  
      return res.end(JSON.stringify(result));
    } catch (err) {
      throw err;
    }
  } else {
    res.statusCode = 404;
    res.end("Not found");
  }
});

server.listen(3789);