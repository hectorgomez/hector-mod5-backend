const myThings = require('./my-things');
const moreThings = require('./more-things');

module.exports = {
  myThings,
  moreThings,
};
