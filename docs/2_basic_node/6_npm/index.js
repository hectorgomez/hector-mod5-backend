require('dotenv').config();

const numbersArray = [1, 2, 3, 4, 5];

const sum = numbers => numbers.reduce((acc, number) => acc + number, 0);

console.log(sum(numbersArray));
console.log(process.env.MY_VARIABLE);