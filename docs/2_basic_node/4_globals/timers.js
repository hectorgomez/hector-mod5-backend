setTimeout(() => {
  console.log('Hello from setTimeout!');
}, 1000);

const interval = setInterval(() => {
  console.log('Hello from setInterval!');
}, 2000);

const timeout = setTimeout(() => {
  console.log('Another timeout!!');
}, 3000);

setTimeout(() => {
  clearTimeout(timeout);
}, 2000);

setTimeout(() => {
  clearInterval(interval);
}, 5000);
