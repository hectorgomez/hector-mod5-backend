const { sayHello } = require('./hello');
const { sayHelloToHackaboss } = require('./hello-hackaboss');

module.exports = {
  sayHello,
  sayHelloToHackaboss,
};