# Autenticación usando JWT

## Qué son los JWT
JWT significa JSON Web Token. Un JWT no es más que un string que tiene tres partes codificadas en Base64, separadas por un punto, más adelante veremos qué significa y para qué usamos cada una de estas partes. Aunque es solamente un token (un string) y en él podemos guardar cualquier tipo de data o metadata, el uso más común para estos tokens es ayudanos a gestionar la autenticación y el acceso de los diferentes usuarios a nuestro sistema.

## Cookies vs JWT
Durante mucho tiempo se han venido usando cookies para autenticar, autorizar y gestionar sesiones de usuarios en servidores. Desde hace unos años, con la popularización de los JWT, su uso a decrecido en favor de estos últimos. Las cookies como mecanismo de gestión de sesiones son **stateful** (con estado), esto es, en el servidor y en el cliente se guarda el mismo pedazo de información sobre el usuario (por lo general un identificador de sesión), mientras que los JWT suelen usarse como mecanismo de autenticación **stateless** (sin estado), es decir, el servidor no guarda información del JWT una vez lo emite, sino que confía en la información del JWT recibido desde el cliente.

Obviamente esta *confianza* del servidor está basada en la criptografía. Cada JWT está firmado con una clave criptográfica que el servidor conoce, y así puede verificar que el contenido del JWT no ha sido alterado por el cliente. 

Aquí vemos un ejemplo típico de comunicación entre cliente y servidor usando cookies, y otro usando JWT. Como vemos, en ambos modos el cliente manda unas credenciales, y se le responde con una cookie o un JWT, según el caso. En las siguientes llamadas el cliente deberá enviar esa cookie o JWT, pero la diferencia radica en que el servidor en el segundo caso no tiene constancia de ninguna sesión de usuario, simplemente verifica que el JWT es correcto en cada llamada.

![alt text](https://1.bp.blogspot.com/-5C7E7LEwX2A/XBPFDI7Y0jI/AAAAAAAAH7o/rfWkNyOw49IIUK4x9FS4329jHnkJ5QXbACLcBGAs/s1600/Cookie-v-Token-Diagram-v1-3-1024x536.png "JWT vs Cookies")

Usar JWT tiene varias ventajas. Como hemos visto no es necesario que el sevidor guarde una sesión para el usuario en memoria/base de datos, por lo cual es más sencillo enviar request a un clúster de servidores (no necesitan tener acceso común a una base de datos de sesiones). También favorece el poder tener por ejemplo un servidor que se encarge de gestionar sólo la autenticación de usuarios y expedir los JWT, para después usar ese JWT en otro servidor del mismo sistema que se encarge de otra tarea, por ejemplo gestionar los pedidos de una tienda online, sin tener que comunicar el sistema de autenticación con el de pedidos, ya que este segundo puede verificar el JWT simplemente teniendo la firma criptográfica.

## Partes de un JWT
Como hemos dicho, un JWT no es más que un string que tiene tres partes: header, payload y signature. 

* **Header**: encabezado dónde se indica, al menos, el algoritmo y el tipo de token.
* **Payload**: información contenida en el token. Por lo general datos de usuario y privilegios.
* **Signature**: firma del token. Permite la comunicación segura entre cliente y servidor.

En la web [jwt.io](https://jwt.io/) existe un codificador/decodificador de JWT muy fácil de usar y que nos puede servir para explorar como se contruyen y deconstruyen JWT.

## JWT en NodeJS
Para trabajar con JWT en el contexto de NodeJS, usaremos la librería de jsonwebtoken de npm: [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken)

Como todos los módulos npm que hemos visto durante el curso, la instalaremos así:
```
npm install jsonwebtoken
```

Esta librería nos facilita dos funciones, que usaremos para firmar y verificar JWT.
```javascript
const jwt = require('jsonwebtoken');
const token = jwt.sign(payload, secret); // esto meterá el payload dentro del token
const payload = jwt.verify(token, secret); // esto leerá el payload del token 
```

Estas dos funciones son así de simples. `jwt.sign` creará un token con la información proporcionada en el payload, y firmará el token usando el `secret` (una clave secreta almacenada en nuestro servidor). Después, para verificar y leer los datos almacenados en ese JWT, podremos hacer uso de la función `jwt.verify`, que además de verificar que el JWT no ha sido alterado desde su creación usando el mismo `secret`, nos devolverá la información guardada en el payload del JWT.