const path = require('path');

console.log(`normalize: ${path.normalize('///home/hector/../hector')}`);
console.log(`join: ${path.join('/home/hector/', 'my-folder')}`);
console.log(`resolve: ${path.join('/home/hector', './my-folder')}`);
console.log(`dirname: ${path.dirname('/home/hector/myFile.pdf')}`);
console.log(`extname: ${path.extname('/home/hector/myFile.pdf')}`);