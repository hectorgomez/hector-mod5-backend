const fs = require('fs').promises;

async function readFile(file) {
  const data = await fs.readFile(file);
  return data.toString();
}

async function main() {
  try {
    const content = await readFile('file.txt');
    console.log('El contenido del fichero es:', content);
  } catch (err) {
    console.error('Hubo un error', err);
    throw err;
  }
}

main();