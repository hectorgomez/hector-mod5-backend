const fs = require('fs');

fs.writeFile('file.txt', 'Hello async world!', function(error) {
  if (error) {
    console.error(error);
  }
  
  console.log('¡Escritura correcta!');
});

console.log('Esto debería imprimirse antes');

const fs = require('fs').promises;

const read = async () => {
  try {
    const content = await fs.readFile('file.txt', 'utf-8');
    console.log(content);
  } catch (err) {
    throw err;
  }
}

read();