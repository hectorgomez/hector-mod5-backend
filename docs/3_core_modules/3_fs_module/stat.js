const fs = require('fs').promises;

async function printStats(filename) {
  const stats = await fs.stat(filename);

  console.log(stats);
  
  if (stats.isFile()) {
    console.log('Es un archivo!');
  }

  if (stats.isDirectory()) {
    console.log('Es un directorio!');
  }
}

printStats('./');
