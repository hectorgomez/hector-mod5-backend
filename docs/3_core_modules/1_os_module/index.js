const os = require('os');

console.log(`totalmem: ${os.totalmem()}`);
console.log(`freemem: ${os.freemem()}`);
console.log(`hostname: ${os.hostname()}`);
console.log(`homedir: ${os.homedir()}`);
console.log(`tmpdir: ${os.tmpdir()}`);
