# Integración con bases de datos SQL

## Trabajando con bases de datos
Hasta ahora hemos visto cómo crear y definir los comportamientos de nuestros servidores Express/NodeJS usando la memoria de nuestra máquina o algún archivo JSON como fuentes de datos en nuestros ejemplos. Por lo general, en un entorno real, estas fuentes de datos se usarán más para tareas puntuales, y para el grueso del tratamiento de nuestros datos usaremos bases de datos.

En este módulo nos centraremos en las bases de datos SQL, que son las más extendidas, pero existen otros tipos de bases de datos (NoSQL) que han ido ganando cuota de mercado aceleradamente en los últimos años. Como ejemplos de bases de datos NoSQL podemos citar las documentales (MongoDB, CouchDB), los almacenes clave-valor (Redis, RocksDB), bases de datos de grafos (Neo4j, OrientDB), etc.

Pero como decíamos en el este curso nos centraremos en integrar una base de datos SQL en nuestros backends, en este caso **MySQL**.

Para acceder a nuestra base de datos MySQL desde nuestro backend escrito en NodeJS, deberemos usar un software que nos haga de *puente* y que nos dé la posibilidad de enviar órdenes a nuestra base de datos. Este software es lo que se conoce como *driver*. Durante el curso usaremos el módulo **mysql2** que podemos instalar usando **npm**. Existe otra implementación del driver, también muy usada, llamada **mysql**, pero no es una primera versión de **mysql2** sino que se trata de módulos independientes mantenidos por equipos independientes; su funcionamiento es muy similar pero por lo general **mysql2** tiene mejor *performance* (es más rápida en la mayoría de escenarios).

Como se trata de un módulo *npm*, lo primero que deberemos hacer es instalarlo en nuestro servidor NodeJS, añadiendo la dependencia a nuestro package.json de la siguiente forma:

```
npm install mysql2
```

Una vez hecho esto, ya podemos empezar a trabajar con MySQL en nuestros proyectos NodeJS.

Por defecto, las funciones de **mysql2** usan callbacks, sin embargo el paquete también incorpora una versión de sus funciones que devuelves promises, por lo que, igual que hacíamos con el módulo **fs** de los core modules de NodeJS, preferiremos trabajar con su versión promises por simplicidad, pudiendo hacer uso de la sintaxis `async/await` en nuestro código.

Así, para requerir el driver, lo haremos con la variante:

```javascript
const mysql = require('mysql2/promise');
```

Asumiendo que estamos corriendo el servidor **MySQL** en nuestra máquina local, dentro de una función asíncrona, podremos inicializar la conexión a nuestra base de datos con:

```javascript
const connection = await mysql.createConnection({
  host: 'localhost',
  database: $nombre_de_la_base_de_datos,
  user: $nombre_del_usuario_de_la_base_de_datos,
  password: $password_del_usuario_de_la_base_de_datos,
});
```

Si queremos podemos aislar la lógica para crear una conexión en un fichero especializado, por ejemplo `database.js`, que exportará una función del tipo:

```javascript
async function getConnection() {
  const connection = await mysql.createConnection({
    host: 'localhost',
    database: $nombre_de_la_base_de_datos,
    user: $nombre_del_usuario_de_la_base_de_datos,
    password: $password_del_usuario_de_la_base_de_datos,
  });

  return connection;
}

module.exports = { getConnection };
```

Así, podremos llamar a esta función desde otra parte de nuestro código y no tendremos que reescribir la lógica de conexión en cada endpoint. Una vez que tenemos una conexión creada, ya podemos empezar a enviarle queries a nuestra base de datos:

```javascript
const result = await connection.query('SELECT * FROM `books`);
```

Cuando usamos promesas, el resultado de la ejecución de la función `connection.query()` puede ser una excepción (un error) o bien puede devolver un array, que tendrá como primer elemento otro array con los resultados de la ejecución y como segundo elemento otro array con la información de las columnas que estén implicadas en la query. Por lo general trabajaremos con el primer elemento, por lo que podemos usar `result[0]` o bien hacer destructuring de esta forma:
```javascript
const [rows] = await connection.query('SELECT * FROM `books`);
```
Recordemos que en este caso `rows` será a su vez un array, por lo que para acceder al elemento N del array habrá que usar `rows[N]`.

Como estamos usando promesas, cualquiera de las funciones que estamos viendo de **mysql** puede resultar en una promesa rechazada, por lo que puede emitir una excepción. Por tanto, para manejar estas excepciones, es recomendable envolver las llamadas a las promesas con sentencias `try/catch`. Por ejemplo, podría tener un endpoint de tipo `GET` en mi servidor que lanzase una query contra la base de datos, y en ese caso me interesaría capturar la posible excepción para notificarla al cliente que está usando mi API.

```javascript
app.get('/books', async (req, res) => {
  try {
    const connection = await database.getConnection();
    const query = 'SELECT * FROM `books`';
    const result = await connection.query(query);
    await connection.end();

    res.send(result[0]);
  } catch (err) {
    res.status(500);
    res.send(err.message)
  }
});
```

Hay que resaltar que cada vez que creamos una conexión en un endpoint, después deberemos cerrar la conexión, en otro caso la conexión quedaría abierta, y cada vez que ejecutemos el endpoint, se abriría una nueva. Para poder lidiar con este problema de una forma más sencilla podemos hacer uso de un **pool de conexiones**. Un pool de conexiones es un conjunto de conexiones que están abiertas indefinidamente y que nuestro driver se encarga de gestionar, cerrándolas y asignándolas dinámicamente según las necesidades de nuestro servidor NoderJS. Para una explicación detallada, ver [Connection Pool](https://es.wikipedia.org/wiki/Connection_pool).

Podemos crear un pool de conexiones con el driver **mysql2** de esta forma:

```javascript
const pool = mysql.createPool({
  host: DATABASE_HOST,
  database: DATABASE_NAME,
  user: DATABASE_USER,
  password: DATABASE_PASSWORD,
});

module.exports = { pool };
```

Y entonces podemos empezar a usar `pool.query()` y `pool.execute()` igual que lo hacemos con `connection.query()` y `connection.execute()`. Automáticamente el driver se encargará de crear y cerrar las conexiones con MySQL por nosotros, por lo que trabajar de esta forma, además de ser más óptimo porque podemos tener múltiples conexiones abiertas en paralelo, es también más cómodo.

```javascript
app.get('/books', async (req, res) => {
  try {
    const query = 'SELECT * FROM `books`';
    const result = await database.pool.query(query);

    res.send(result[0]);
  } catch (err) {
    res.status(500);
    res.send(err.message)
  }
});
```

En nuestras queries, muy frecuentemente querremos usar parámetros, por ejemplo dentro de nuestras sentencias `WHERE`, para ello usaremos el caracter de escape `?` y pasaremos como segundo argumento un array que contendrá los valores por lo que queremos sustituir las `?`:

```javascript
const author = 'Franz Kafka';
const result = await pool.query('SELECT * FROM `books` WHERE `author` = ?', [author]);
```

En lugar de un array como segundo parámetro también podemos mandar una enumeración de variables:
```javascript
const author = 'Franz Kafka';
const result = await pool.query('SELECT * FROM `books` WHERE `author` = ? AND `title = ?`', author, title);
```

Análogamente para queries de inserción o actualización de datos:
```javascript
const values = [
  ['Miguel Delibes', 'El camino'],
  ['Leon Tolstoi', 'Guerra y Paz'],
  ['Truman Capote', 'A sangre fría'],
];
const result = await pool.query('INSERT INTO `books` VALUES ?', values);
```


```javascript
const values = ['Los hermanos Karamazov', 1];
const result = await pool.query('UPDATE `books` SET title = ? WHERE id = ?', values);
```
```javascript
const values = ['Fiodor Dostoievski', 'Los hermanos Karamazov', 1];
const result = await pool.query('UPDATE `books` SET author = ?, title = ? WHERE id = ?', values);
```

## Aislando los accesos a orígenes de datos en repositorios
Como hemos estado viendo, es común que una determinada operación la hagamos en diferentes controllers, es decir, que tengamos que realizarla para responder a varias peticiones distintas. Por ejemplo podríamos querer recuperar la información del usuario en múltiples peticiones de éste a nuestro backend.

Para factorizar esos accesos, y así poder reusarlos, usamos lo que normalmente se denomina **repositorios**. Los repositorios no son más que agrupaciones lógicas de funciones que normalmente trabajan con el mismo origen de datos. Siguiendo el ejemplo anterior de recuperación de información del usuario, podemos tener un `user-repository` que agrupará todas las funciones que lean o escriban información de los usuarios a nuestro origen de datos (que durante el curso será una base de datos SQL).

```javascript
async function findUsers() {}
async function findUserById(id) {}
async function findUserByName(name) {}
async function addUser(data) {}
async function updateUser(data) {}
async function deleteUserById(id) {}

module.exports = {
  findUsers,
  findUserById,
  findUserByName,
  addUser,
  updateUser,
  deleteUserById,
};
```

Así por ejemplo podríamos usar la función `findUserById` en múltiples controllers sin necesidad de escribir su código en todos ellos, favoreciendo la consistencia y la reusabilidad de nuestro código: [DRY](https://es.wikipedia.org/wiki/No_te_repitas).

Separar nuestro software en capas tiene otra ventaja: unas capas no dependen de las otras y sus implementaciones son intercambiables. Por ejemplo hemos visto al principio de este tema que existen diferentes tipos de bases de datos. Si por ejemplo en algún momento queremos migrar nuestros datos desde nuestra base de datos MySQL a otra base de datos NoSQL, sólo tendríamos que adaptar nuestros repositorios. Nuestros controllers seguirían funcionando igual porque el acceso a datos lo tenemos aislado en la capa de repositorios. Más información sobre este tema: [SOLID](https://es.wikipedia.org/wiki/SOLID).
